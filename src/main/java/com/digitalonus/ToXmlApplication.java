package com.digitalonus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

@SpringBootApplication
@EnableJpaRepositories
public class ToXmlApplication {

	@Bean
	public ObjectMapper objectMapper() {
		return new XmlMapper();
	}

	public static void main(String[] args) {
		SpringApplication.run(ToXmlApplication.class, args);
	}
}