package com.digitalonus.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
// @Table(name = "SAP_PI_XML_QUEUE")
@Table(schema = "SORAAPEX", name = "SAP_PI_XML_QUEUE")
public class Model {
	@Id
	@Column(name = "SAP_PI_XML_QUEUE_UID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Lob
	@Column(name = "MESSAGE_CLOB")
	private String messageClob;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessageClob() {
		return messageClob;
	}

	public void setMessageClob(String messageClob) {
		this.messageClob = messageClob;
	}
}