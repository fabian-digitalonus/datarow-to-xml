package com.digitalonus.domain;

import org.springframework.data.repository.CrudRepository;

public interface ModelRepository extends CrudRepository <Model, Integer>
{
   
}