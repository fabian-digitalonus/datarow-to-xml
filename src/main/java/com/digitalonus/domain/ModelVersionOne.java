package com.digitalonus.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@Entity
@Table(name = "SAP_PI_XML_QUEUE")
// @Table (schema = "SORAAPEX", name = "SAP_PI_XML_QUEUE")
@JacksonXmlRootElement(localName = "yq1:MES_OperationConfirmation_MT")
public class ModelVersionOne {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String lotNumber;
	private String identifier;
	private String material;
	private String operationNumber;
	private String confirmationDate;
	private String yield;
	private String scrap;
	private String storageLocation;
	private String plant;
	private String uom;

	// - Namespaces =================
	// Only for the XML serialization. Kinda useless.

	@JacksonXmlProperty(localName = "xmlns:SOAP-ENV", isAttribute = true)
	public String getNamespaceOne() {
		return "http://schemas.xmlsoap.org/soap/envelope/";
	}

	@JacksonXmlProperty(localName = "xmlns:xs", isAttribute = true)
	public String getNamespaceTwo() {
		return "http://www.w3.org/2001/XMLSchema";
	}

	@JacksonXmlProperty(localName = "xmlns:xsi", isAttribute = true)
	public String getNamespaceThree() {
		return "http://www.w3.org/2001/XMLSchema-instance";
	}

	// - End of namespaces ==========

	@JsonIgnore
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("LOT_Number")
	public String getLotNumber() {
		return lotNumber;
	}

	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}

	@JsonProperty("Identifier")
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	@JsonProperty("Material")
	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	@JsonProperty("Operation_number")
	public String getOperationNumber() {
		return operationNumber;
	}

	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}

	@JsonProperty("Confirmation_date")
	public String getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(String confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	@JsonProperty("Yield")
	public String getYield() {
		return yield;
	}

	public void setYield(String yield) {
		this.yield = yield;
	}

	@JsonProperty("Scrap")
	public String getScrap() {
		return scrap;
	}

	public void setScrap(String scrap) {
		this.scrap = scrap;
	}

	@JsonProperty("Storage_location")
	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	@JsonProperty("Plant")
	public String getPlant() {
		return plant;
	}

	public void setPlant(String plant) {
		this.plant = plant;
	}

	@JsonProperty("UOM")
	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}
}