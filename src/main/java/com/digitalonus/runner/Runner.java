package com.digitalonus.runner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.digitalonus.domain.Model;
import com.digitalonus.domain.ModelRepository;

@Component
public class Runner implements CommandLineRunner {

	private final static transient Logger log = Logger
			.getLogger(CommandLineRunner.class);

	@Inject
	private ModelRepository modelRepository;

	// @Inject
	// private ObjectMapper objectMapper;

	public void run(String... args) throws IOException {
		String errorString = null;
		int id = 0;
		String fileName = "";

		log.info("=============================");
		log.info("=========== Start ===========");

		if (args.length < 1) {
			errorString = "You must pass the Id of the row to retrieve.";
		}

		if (null == errorString) {
			try {
				id = Integer.valueOf(args[0]);

			} catch (NumberFormatException nfe) {
				errorString = "Argument must be a positive integer.";
			}
		}

		if (null == errorString) {
			fileName = "exportedXML_" + id + ".xml";
			Model model = modelRepository.findOne(id);

			if (null == model) {
				errorString = "The Id was not found in the table.";
			} else {
				File file = new File(fileName);
				FileWriter writer = new FileWriter(file);
				writer.write(model.getMessageClob());
				writer.close();
			}
		}

		if (null != errorString) {
			log.error("There is an error in your execution.");
			log.error(errorString);
		} else {
			log.info("Success! Your row was exported in file " + fileName);
		}

		log.info("===========  End  ===========");
		log.info("=============================");

	}
}